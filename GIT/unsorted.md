Show uncommited files

```
git diff
```

Revert all uncommited changes

```
git checkout .
```

Show latest commit message

```
git log -1 --pretty=%B
```

Show current branch name

```
git name-rev HEAD
```