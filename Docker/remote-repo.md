[DOCKER HUB](https://hub.docker.com/)

▸ Register a new Docker Hub account

▸ Log in via terminal

`docker login`

```
Authenticating with existing credentials...
Login Succeeded
```

▸ In case you need to switch Docker Hub account

Log out via terminal first

`docker logout`

Then log in via terminal

`docker login`

[https://docs.docker.com/engine/reference/commandline/login/](https://docs.docker.com/engine/reference/commandline/login/)

```
--password , -p 		Password
--password-stdin 		Take the password from stdin
--username , -u 		Username
```

---

▸ Run `docker-compose up` and install everything you need

▸ Run `docker ps -a`

Find out required CONTAINER ID

▸ Run `docker commit [CONTAINER ID] [New image full name]` 

__I.E__: `docker commit d870eaecd624 phpkit/server-lite:php8.0`

```
sha256:5b2e710d51cfd9ab943de9f21b2f960f182856dce8cdd1f46b3692ebd8073302
```

▸ Run `docker image push phpkit/server-lite:php8.0`

```
denied: requested access to the resource is denied
```

This message might appear if you did not log out while switching Docker Hub account t via terminal
